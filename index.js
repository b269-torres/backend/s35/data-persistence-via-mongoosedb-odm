const express = require("express");

// Mongoose is a package that allows creating of Schemas to out model our sata structure
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");
const app = express();
const port = 3001;

// Connecting to Mongodb Atlas
mongoose.connect("mongodb+srv://reybutchtorres:admin123@zuitt-bootcamp.p8hs8ti.mongodb.net/s35?retryWrites=true&w=majority",
	{
		// allows us to avoid any current and future errors while connecting to MongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true
	}

);

//Allows to handle errors when initial connection is established.
let db = mongoose.connection;

//console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));
// If the connection is successful, out in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Connecting to Mongodb Atlas END

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//[Section] Mongoose Schemas
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});


// [ Section] Mongoose model
// Models must be in singular form and capitalized
const Task = mongoose.model("Task", taskSchema);


// Creation of Task Application
// Crate a new task
/*
BUSENESS LOGIC
1.Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database
2.The task data will be coming from the request's body
3.Create a new Task object with a "name" field/property
4.The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
app.post("/tasks", (req, res) =>{
Task.findOne({name: req.body.name}).then((result,err) => {
	if(result != null && result.name == req.body.name){
		return res.send("Duplicate task found");
	} else {
		let newTask = new Task({
			name: req.body.name
		});
		newTask.save().then((savedTask, saveErr)=> {
			if(saveErr){
				return console.error(saveErr);
			} else {
				return res.status(201).send("New task created!");
			}
		})
	}
})
});


// Getting all the tasks
/*

BUSINESS LOGIC
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/



app.get("/tasks", (req, res) =>{
	Task.find({}).then((result, err) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
});





app.listen(port, () => console.log(`Server running at port ${port}`));


//activity s35

// Other codes are in the top so i just copy the needed code for the activity



const userSchema = new mongoose.Schema({
	username: String,
	password: String
});


const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) =>{
User.findOne({username: req.body.username}, {password: req.body.password}).then((result,err) => {
	if(result != null && result.name == req.body.name || result != null && result.password == req.body.password){
		return res.send("Duplicate task found");
	} else {
		let newUser = new User({
			username: req.body.username,
			password: req.body.password
		})
		newUser.save().then((savedTask, saveErr)=> {
			if(saveErr){
				return console.error(saveErr);
			} else {
				return res.status(201).send("New task created!");
			}
		})
	} 
})
});


app.get("/signup", (req, res) =>{
	User.find({}).then((result, err) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
});
